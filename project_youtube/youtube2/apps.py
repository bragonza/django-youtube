from django.apps import AppConfig
from .ytparser import YTCanal

import urllib.request

class Youtube2Config(AppConfig):
    name = 'youtube2'

    def ready (self):
        from .models import No_Seleccionados
        No_Seleccionados = self.get_model('No_Seleccionados')
        Seleccionados = self.get_model('Seleccionados')
        url = "https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg"
        xmlStream = urllib.request.urlopen(url)
        canal = YTCanal(xmlStream)
        datos = canal.videos()
        no_seleccionados = No_Seleccionados.objects.all()
        no_seleccionados.delete()
        seleccionados = Seleccionados.objects.all()
        seleccionados.delete()
        for video in datos:
            c = No_Seleccionados(id_video=video['id_video'], titulo=video['title'], link=video['link'],
                                 name_canal=video['name_canal'], date=video['date'], description=video['description'],
                                 photo=video['photo'], url_canal=video['url_canal'])
            c.save()