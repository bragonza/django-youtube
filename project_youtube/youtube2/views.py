from django.shortcuts import get_object_or_404, render
from .models import Seleccionados, No_Seleccionados
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.utils import timezone


@csrf_exempt
def index(request):
    if request.method == "POST":
        # Coger el valor que haya en el cuerpo de HTTP
        action = request.POST['action']
        id_video = request.POST['id']

        if action == "Añadir":

            video = No_Seleccionados.objects.get(id=id_video)
            seleccionados = Seleccionados(titulo=video.titulo, id_video=video.id_video, link=video.link,
                                          name_canal=video.name_canal, description=video.description, photo=video.photo,
                                          url_canal=video.url_canal, date=video.date)
            seleccionados.save()
            video.delete()

        elif action == "Eliminar":
            video = Seleccionados.objects.get(id=id_video)
            no_seleccionados = No_Seleccionados(titulo=video.titulo, id_video=video.id_video, link=video.link,
                                                name_canal=video.name_canal, description=video.description,
                                                photo=video.photo,
                                                url_canal=video.url_canal, date=video.date)
            no_seleccionados.save()
            video.delete()

    seleccionados = Seleccionados.objects.all()
    no_seleccionados = No_Seleccionados.objects.all()
    context = {'seleccionados': seleccionados,
               'no_seleccionados': no_seleccionados}
    #return HttpResponse (template.render(context,request))
    return render(request, "youtube/index.html", context)

def get_video(request, llave):
    try:
        video = Seleccionados.objects.get(id_video=llave)
        context = {'video': video,
                'id': llave}
    except Seleccionados.DoesNotExist:
        video = ""
        context = {'video':video,
                   'id': llave}


    return render(request, "youtube/get_video.html", context)
