from django.db import models

# Create your models here.
class Seleccionados(models.Model):
	
	titulo = models.CharField(max_length=128)
	id_video = models.CharField(max_length=64)
	link = models.TextField()
	date = models.CharField(max_length=128)
	name_canal = models.CharField(max_length=128)
	description = models.CharField(max_length=128)
	photo = models.CharField(max_length=128)
	url_canal = models.CharField(max_length=128)

	def __str__(self):
		return self.id_video + " = " + self.titulo

class No_Seleccionados(models.Model):

	titulo = models.CharField(max_length=128)
	id_video = models.CharField(max_length=64)
	link = models.TextField()
	date = models.CharField(max_length=30)
	name_canal = models.CharField(max_length=128)
	description = models.CharField(max_length=128)
	photo = models.CharField(max_length=64)
	url_canal = models.CharField(max_length=128)

	def __str__(self):
		return self.id_video + " = " + self.titulo

