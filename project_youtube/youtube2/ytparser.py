#!/usr/bin/python3

#
# Simple XML parser for YouTube XML channels
# Jesus M. Gonzalez-Barahona
# jgb @ gsyc.es
# SARO and SAT subjects (Universidad Rey Juan Carlos)
# 2020
#
# Produces a HTML document in standard output, with
# the list of videos on the channel
#
# How to get the XML document for a YouTube channel:
# https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import urllib
import string


class YTHandler(ContentHandler):

    def __init__(self):

        self.inEntry = False
        self.inContent = False
        self.inAuthor = False
        self.inMedia = False

        self.content = ""
        self.id_video = ""
        self.title = ""
        self.link = ""
        self.name_canal = ""
        self.url_canal = ""
        self.date = ""
        self.description = ""
        self.photo = ""
        self.videos = []

    def startElement (self, name, attrs):
        if name == 'entry':
            self.inEntry = True
        elif self.inEntry:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.link = attrs.get('href')
            elif name == 'yt:videoId':
                self.inContent = True
            elif name == 'published':
                self.inContent = True

            elif name == 'author':
                self.inAuthor = True
            elif name == 'media:group':
                self.inMedia = True

            elif self.inAuthor:
                if name == 'name':
                    self.inContent = True
                elif name == 'uri':
                    self.inContent = True
            elif self.inMedia:
                if name == 'media:description':
                    self.inContent = True
                elif name == 'media:thumbnail':
                    self.photo = attrs.get('url')

    def endElement (self, name):
        global videos

        if name == 'entry':
            self.inEntry = False
            self.videos.append ({'id_video': self.id_video,
                                 'title': self.title,
                                 'link': self.link,
                                 'name_canal': self.name_canal,
                                 'url_canal': self.url_canal,
                                 'date': self.date,
                                 'description': self.description,
                                 'photo': self.photo})
        elif self.inEntry:
            if name == 'title':
                self.title = self.content
                self.content = ""
                self.inContent = False
            elif name == 'yt:videoId':
                self.id_video = self.content
                self.content = ""
                self.inContent = False
            elif name == 'author':
                self.inAuthor = False
            elif self.inAuthor:
                if name == 'name':
                    self.name_canal = self.content
                    self.content = ""
                    self.inContent = False
                elif name == 'uri':
                    self.url_canal = self.content
                    self.content = ""
                    self.inContent = False
            elif name == 'published':
                self.date = self.content
                self.content = ""
                self.inContent = False

            elif name == 'media:group':
                self.inMedia = False
            elif self.inMedia:
                if name == 'media:description':
                    self.description = self.content
                    self.content = ""
                    self.inContent = False

    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars


class YTCanal:
    def __init__ (self, stream):
        self.parser = make_parser()
        self.handler = YTHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def videos (self):
        return self.handler.videos